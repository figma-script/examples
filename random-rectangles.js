/*
*   by kay-n
*
*   The script creates a frame and 100 rectangles in it
*   with random coordinates, sizes and fill color
*/


/*
*   The function returns a random integer
*   from 0 to the number passed as a parameter
*/
function getRandomInt(maxValue = 1) {
    return Math.floor(Math.random() * maxValue)
}

/*
*   The function returns an object with width and height properties,
*   with random values from 1 to the value
*   of the maxWidth and maxHeight parameters
*/
function getRandomSize(maxWidth=200, maxHeight=200) {
    return {
        width: 1 + getRandomInt(maxWidth - 1),
        height: 1 + getRandomInt(maxHeight - 1)
    };
}

/*
*   The function returns an object with x and y properties,
*   with random values within the height and width of the frame
*/
function getRandomCoordinates() {
    return {
        x: getRandomInt(frame.width),
        y: getRandomInt(frame.height)
    };
}

/*
*   The function returns an object with random values
*   for properties r, g and b ranging from 0 to 1
*/
function getRandomFill() {
    return { r: Math.random(), g: Math.random(), b: Math.random() };
}

/*
*   The function creates a rectangle, gives it the specified dimensions,
*   coordinates, and fill, and places the rectangle in a frame
*/
function drawRect(size, coords, fillColor) {
    const rect = figma.createRectangle();
    rect.resize(size.width, size.height);
    rect.x = coords.x;
    rect.y = coords.y;
    rect.fills = [{ type: 'SOLID', color: fillColor }];
    
    frame.appendChild(rect)   
}


//  Create a 1000*500px frame
const frame = figma.createFrame();
frame.resize(1000, 500);


//  Creating Rectangles
for (let i = 0; i < 100; i++) {
    const size = getRandomSize();
    const coords = getRandomCoordinates();
    const fillColor = getRandomFill();
    
    drawRect(size, coords, fillColor);
}
