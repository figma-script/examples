/*
*   by kay-n
*
*   The script create fractal Sierpinski triangle
*
*   Be careful!!! The complexity of executing
*   the "createFractal" function is O(3^depth)
*   and with a large value of the "depth" parameter
*   it can take considerable time
*/

async function getFrameByName(frameName) {
  if (!figma.currentPage) {
    return
  }

  await figma.currentPage.loadAsync()

  const frame = figma.currentPage
      .findOne(node => node.name === frameName) ||
    (() => {
      const frame = figma.createFrame()
      frame.name = frameName
      return frame
    })()

  return frame
}

function clearFrame(frame){
  frame.children.forEach(c => c.remove())
}
function createFractal({ frame, depth, triangle, size }){
  if(depth === 0) {
    drawTriangle(frame, triangle, size)
  } else {
    let smallTriangle = calculateMidpointTriangle(triangle)
    for (let i = 0; i < triangle.length; i++) {
      createFractal({
        frame,
        size,
        depth: depth - 1,
        triangle: [triangle[i], smallTriangle[0], smallTriangle[2]],
      })
      smallTriangle = [smallTriangle[1], smallTriangle[2], smallTriangle[0]]
    }
  }
}
function getStartTriangle(size){
  const base = { x: size / 2, y: 0 }
  const leftVertex = { x: 0, y: (size / 2) * Math.sqrt(3) }
  const rightVertex = { x: size, y: (size / 2) * Math.sqrt(3) }

  return [base, leftVertex, rightVertex]
}
function drawTriangle(frame, points, size) {
  const path = figma.createVector()
  path.vectorPaths = [{
    windingRule: "EVENODD",
    data: `M ${points[0].x} ${points[0].y} L ${points[1].x} ${points[1].y} L ${points[2].x} ${points[2].y} Z`,
  }]
  const fill = [{ type: 'SOLID', color: getColorByPoint(points[0], size) }]
  path.fills = fill
  path.strokes = [{ ...fill[0], visible: false }]

  frame.appendChild(path)
}
function calculateMidpointTriangle(points) {
  const midAB = { x: (points[0].x + points[1].x) / 2, y: (points[0].y + points[1].y) / 2 };
  const midBC = { x: (points[1].x + points[2].x) / 2, y: (points[1].y + points[2].y) / 2 };
  const midCA = { x: (points[2].x + points[0].x) / 2, y: (points[2].y + points[0].y) / 2 };

  return [midAB, midBC, midCA]
}
function getColorByPoint(point, size) {
  const normalizedX = point.x / size;
  const normalizedY = 1 - point.y / size;

  const r = Math.sin(Math.PI * normalizedY);
  const g = Math.sin(Math.PI * normalizedX);
  const b = Math.sin(Math.PI * normalizedX * normalizedY);

  return { r, g, b };
}


(async () => {
  const size = 1000
  const frame = await getFrameByName('FractalContainer')

  if (!frame) {
    alert('The Figma page is not open. Open or select the page and run the script again.')
    return
  }

  try {
    frame.resize(size, size)

    clearFrame(frame)
    createFractal({
      frame,
      depth: 7,
      triangle: getStartTriangle(size),
      size,
    })
  } catch (e) {
    alert('Something went wrong. Try running the script again')
  }
})()