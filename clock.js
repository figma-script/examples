/*
*   by kay-n
*
*   The script creates and animates a clock with hands
*/

figma.loadFontAsync({ family: "Inter", style: "Regular" })
    .then(init)

function clearFrame(frame) {
    frame.children.forEach(c => c.remove())
}

function pointOnCircle(centerX, centerY, radius, angle) {
    const angleRadians = (angle - 90) * Math.PI / 180.0;

    const x = centerX + (radius * Math.cos(angleRadians));
    const y = centerY + (radius * Math.sin(angleRadians));

    return { x, y };
}

function createClockFace(cont) {
    const centerX = cont.width / 2;
    const centerY = cont.height / 2;
    const angleStep = 360 / 12;
    const R = cont.width / 2 - 100;

    for (let i = 1; i <= 12; i++) {
        const int = figma.createText();

        int.characters = `${i}`
        int.fontSize = 42
        int.fills = [{
            type: 'SOLID',
            color: { r: 0, g: 0, b: 0 }
        }]

        const point = pointOnCircle(centerX, centerY, R, angleStep * i);

        int.x = point.x - int.width * 0.5;
        int.y = point.y - int.height * 0.5;

        cont.appendChild(int);
    }
}

function createClockHands(frame) {
    const minute = createRect(frame, 300, 10, { r: 0, g: 0, b: 0 });
    const hour = createRect(frame, 200, 20, { r: 0, g: 0, b: 0 });
    const second = createRect(frame, 300, 3, { r: 1, g: 0, b: 0 });

    return { minute, hour, second };
}
function createRect(cont, width, height, fillColor) {
    const line = figma.createRectangle();
    line.resize(width, height);
    line.fills = [{ type: 'SOLID', color: fillColor }];
    line.x = cont.width / 2
    line.y = cont.height / 2

    cont.appendChild(line)

    return line;
}

function setTime(clockHands) {
    const date = new Date();

    const hours = date.getHours() % 12;
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();

    clockHands.hour.rotation = 90 - hours * 30 - minutes / 2;
    clockHands.minute.rotation = 90 - minutes * 6;
    clockHands.second.rotation = 90 - seconds * 6;
}

async function init() {
    await figma.currentPage.loadAsync()
    const frame = figma.currentPage
            .findOne(node => node.name === 'ClockContainer') ||
        (() => {
            const frame = figma.createFrame();
            frame.name = 'ClockContainer';
            frame.resize(1000, 1000)
            return frame;
        })();
    clearFrame(frame)
    createClockFace(frame)
    const clockHands = createClockHands(frame)
    setTime(clockHands)
    const interval = setInterval(() => {
        try {
            setTime(clockHands)
        } catch (e) {
            clearInterval(interval)
        }
    }, 1000);
}


