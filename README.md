# Examples for FigmaScript plugin

## random-rectangles.js

by kay-n

The script creates a frame and 100 rectangles in it with random coordinates, sizes and fill color

##
## bubble-animation

by kay-n

The script creates 10 circles in the frame and animates their movement within the frame

##
## clock

by kay-n

The script creates and animates a clock with hands

##
## sierpinski-triangle

The script create fractal Sierpinski triangle