/*
*   by kay-n
*
*   The script creates 10 circles in the frame
* and animates their movement within the frame
*/

const animationStep = 5
const PI2 = Math.PI * 2

const getRandomInt = (maxValue = 1) => Math.floor(Math.random() * maxValue)

//  Create a circle
const getCircle = frame => {
    const circle = figma.createEllipse()
    circle.resize(150, 150)

    circle.x = getRandomInt(frame.width)
    circle.y = getRandomInt(frame.height)

    circle.opacity = 0.5

    circle.fills = [{ type: 'SOLID', color: { r: Math.random(), g: Math.random(), b: Math.random() } }];

    frame.appendChild(circle)

    return {
        shape: circle,
        angle: -Math.PI + Math.random() * PI2
    }
}

//  Change circle coordinates
const changeCoordinates = (circle, frame) => {
    circle.x = circle.shape.x + animationStep * Math.cos(circle.angle)
    circle.y = circle.shape.y + animationStep * Math.sin(circle.angle)

    circle.resultX = circle.x > frame.width - circle.shape.width
        ? frame.width - circle.shape.width : circle.x < 0 ? 0 : circle.x

    circle.resultY = circle.y > frame.height - circle.shape.height
        ? frame.height - circle.shape.height : circle.y < 0 ? 0 : circle.y

    if (circle.x !== circle.resultX) {
        circle.angle = Math.PI - circle.angle
    }

    if (circle.y !== circle.resultY) {
        circle.angle = PI2 - circle.angle
    }

    circle.shape.x = circle.resultX
    circle.shape.y = circle.resultY
}

//  Running Animation
const animation = (circles, frame) => {
    return () => {
        try {
            for (const circle of circles) {
                changeCoordinates(circle, frame)
            }
        } catch (e) {
            clearInterval(interval)
        }
    }
}

const frame = figma.createFrame()
frame.resize(1000, 800)

const circles = []

for (let i = 0; i < 10; i++) {
    circles.push(getCircle(frame))
}

const interval = setInterval(animation(circles, frame), 40)

